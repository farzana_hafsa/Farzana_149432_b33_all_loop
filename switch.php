<?php
$A=5;
switch ($A)
{
    case "1":
        echo  “The number is 1”;
		break;
    case "2":
        echo “The number is 2”;
		break;
    case "3":
        echo “The number is 3”;
		break;
    case "4":
        echo “The number is 4”;
		break;
    case"5":
        echo “The number is 5”;
		break;
    default:
        echo “Non of the numbers match”;
}
?>


<?php
$favcolor = "red";

switch ($favcolor) {
    case "red":
        echo "Your favorite color is red!";
        break;
    case "blue":
        echo "Your favorite color is blue!";
        break;
    case "green":
        echo "Your favorite color is green!";
        break;
    default:
        echo "Your favorite color is neither red, blue, nor green!";
}
?>

